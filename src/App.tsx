import './App.css'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import { useTheme } from './contexts/ThemeContext'
import Header from './components/Header'
import Home from './pages/Home'
import About from './pages/About'
import Work from './pages/Work'
import Contact from './pages/Contact'

function App() { 
  const { darkMode } = useTheme();  

  return (
    <Router>
        <div className={`body ${darkMode ? 'dark' : 'light'}`}>
          <Header />
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/about" element={<About />} />
            <Route path="/work" element={<Work />} />
            <Route path="/contact" element={<Contact />} />
          </Routes>
        </div>
    </Router>
  )
}

export default App
