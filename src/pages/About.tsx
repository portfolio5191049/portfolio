import React from "react";
import AboutContent from "../components/About";

const About: React.FC = () => {
  return (
    <div>
      <AboutContent />
    </div>
  );
};

export default About;