import React from "react";
import WorkContent from "../components/Work";

const Work: React.FC = () => {
  return (
    <div>
      <WorkContent />
    </div>
  );
};

export default Work;