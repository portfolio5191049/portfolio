import React from "react";
import ContactContent from "../components/Contact";

const Contact: React.FC = () => {
  return (
    <div>
      <ContactContent />
    </div>
  );
};

export default Contact;