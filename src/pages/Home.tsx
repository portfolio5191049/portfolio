import React from 'react';
import Homecontent from '../components/Home';

const Home: React.FC = () => {
  return (
    <div>
      <Homecontent />
    </div>
  );
};

export default Home;
