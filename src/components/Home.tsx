import React from "react";
import { Link } from "react-router-dom";

const Home: React.FC = () => {
  return (
    <div className="homeContainer">
      <h1>🚀 ¡Welcome to my digital playground!</h1>
      <p>
        I'm Adriana, a passionate software developer dedicated to turning
        innovative ideas into reality. With a love for coding and a knack for
        problem-solving, I specialize in crafting solutions that bring digital
        concepts to life. Explore my portfolio to witness the synergy of
        creativity and technology. Let's build the future together!
      </p>
      <p>
        <span>#TechEnthusiast </span>
        <span>#CodeCrafting </span>
        <span>#InnovationJourney </span>
      </p>
      <Link to="/contact">
        <button>CONTACT ME</button>
      </Link>
    </div>
  );
};

export default Home;
