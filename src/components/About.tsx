import React from "react";

const About: React.FC = () => {
  return (
    <div>
      <h1>Welcome to the About component!</h1>
      <p>This is the about page of your application.</p>
    </div>
  );
};

export default About;