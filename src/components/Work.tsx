import React from "react";

const Work: React.FC = () => {
  return (
    <div>
      <h1>Welcome to the Work component!</h1>
      <p>This is the work page of your application.</p>
    </div>
  );
};

export default Work;