import React from 'react';
import { Link } from 'react-router-dom';
import { useTheme } from '../contexts/ThemeContext';
import Moon from '../assets/moon.svg';
import Sun from '../assets/sun.svg';

const Header: React.FC = () => {
  const pages = [
    { to: '/', title: 'Home' },
    { to: '/about', title: 'About' },
    { to: '/work', title: 'Work' },
    { to: '/contact', title: 'Contact' },
  ];
  const { darkMode, toggleTheme } = useTheme();

  return (
    <header className="header">
      <nav>
        <ul>
          {pages.map((page, index) => (
            <li key={index}>
              <Link to={page.to}>{page.title}</Link>
            </li>
          ))}
        </ul>
        <button className='toggleTheme' onClick={toggleTheme}>
          <img src={darkMode ? Sun : Moon}></img>
        </button>
      </nav>
    </header>
  );
};

export default Header;

