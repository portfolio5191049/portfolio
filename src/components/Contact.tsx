import React from "react";

const Contact: React.FC = () => {
  return (
    <div>
      <h1>Welcome to the Contact component!</h1>
      <p>This is the contact page of your application.</p>
    </div>
  );
};

export default Contact;